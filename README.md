# House Price Prediction


This is an implementation of an Artificial Neural Network (ANN) to predict house price given the number of rooms as features

# Neural Network parameters:

-  Layers:

    - Dense layer with one unit

-  Loss function:

    - Mean Squared Error (MSE) 

    ![ALT](https://i0.wp.com/statisticsbyjim.com/wp-content/uploads/2021/11/mse_formula.png?resize=246%2C78&is-pending-load=1#038;ssl=1)

-   Optimizer:

    - Stochastic Gradient Descent (SGD)

    ![ALT](https://www.geeksforgeeks.org/wp-content/ql-cache/quicklatex.com-7d9ca0a2c94d45c016518b93df86895e_l3.svg?resize=246%2C78&is-pending-load=1#038;ssl=1)

